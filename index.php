<?php

require __DIR__ . '/autoload.php';

$route = \App\Router::locate();

try {
    if (false !== $route) {
        $controller = new $route['controller'];
        $controller->action($route['action']);
    } else {
        throw new \App\Exceptions\Http404Exception('Страница ' . $_SERVER['REQUEST_URI'] . ' не найдена');
    }

}  catch (\App\Exceptions\Http404Exception $exception) { // Исключения при работе с 404 ошибкой

    $controller = new \App\Controllers\Error();
    $controller->action('404', $exception);

    // добавляем в лог исключение 404
    $logger = new \App\ErrorLogger();
    $logger->log($exception);

} catch (\Exception $exception) {   // все остальное - ошибки БД

    // добавляем в лог исключения которые должны логироваться по умолчанию.
    // в данном случае в это условие попадут исключения Db
    if ($exception instanceof \App\Exceptions\LogableExceptions) {
        $logger = new \App\ErrorLogger();
        $logger->log($exception);
    }

    $controller = new \App\Controllers\Error();
    $controller->action('Default', $exception);

}