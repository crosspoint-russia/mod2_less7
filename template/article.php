<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $article->title; ?></title>
</head>
<body>
<h1><?php echo $article->title; ?></h1>
<p><?php echo $article->text; ?></p>

<p>

    <!-- загружаем модель Author в переменную, для исключения последующих запросов в БД -->
    
    <?php if (false !== ($currentAuthor = $article->author)) { ?>
        Автор:  <?php echo $currentAuthor->firstname . ' ' . $currentAuthor->lastname; ?>
    <?php } else { ?>
        Редакционная статья
    <?php } ?>
</p>

<a href="/">main page</a>
</body>
</html>