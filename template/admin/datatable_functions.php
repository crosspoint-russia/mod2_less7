<?php

$view = new \App\View();

return [
    'Id' => function(\App\Model $article) {
        return $article->id;
    },
    'Название' => function(\App\Model $article) {
        return $article->title;
    },
    'Текст' => function(\App\Model $article) {
        return trim(mb_substr($article->text, 0, 150)) . '...';
    },
    'Автор' => function(\App\Model $article) use ($view) {
        $view->article = $article;
        return $view->render(__DIR__ . '/datatable_author_info.php');
    },
    // на текущий момент во $view уже загружены все необходимые данные
    // т.к. объекты передаются по ссылке
    'Управление' => function() use ($view) {
        return $view->render(__DIR__ . '/datatable_articles_link.php');
    },
];
