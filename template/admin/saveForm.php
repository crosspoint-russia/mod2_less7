<?php $article = $article ?? new \App\Models\Article(); ?>
<form action="/admin/save/?passphrase=iddqd" method="post">
    <div class="form-group">
        <label>Заголовок</label>
        <input type="hidden" name="id" value="<?php echo $article->id;  ?>" class="form-control">
        <input type="text" name="title" value="<?php echo $article->title; ?>" class="form-control">
    </div>
    <div class="form-group">
        <label>Текст:</label>
        <textarea name="text" cols="30" rows="10" class="form-control"><?php echo $article->text; ?></textarea>
    </div>
    <div class="form-group">
    <label>Автор</label>
    <select name="author_id" class="form-control">
        <option value="0">пустой author_id</option>

        <?php
        foreach ($authors as $author) : ?>
        <option value="<?php echo $author->id; ?>"

        <?php if (isset($article->author_id) && $author->id === $article->author_id): ?>
                selected="selected"
        <?php endif; ?>
        >
        <?php echo $author->firstname . ' ' . $author->lastname; ?>
        </option>
        <?php endforeach; ?>

    </select>
    </div>
    <button type="submit" class="btn btn-success">->save()</button>
</form>