<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 24.01.2017
 * Time: 23:22
 */

namespace App;


abstract class AController extends Controller
{
    protected function access():bool
    {
        // заглушка проверки прав доступа
        if (isset($_GET['passphrase']) && 'iddqd' === $_GET['passphrase']) {
            return true;
        } else {
            return false;
        }
    }
}