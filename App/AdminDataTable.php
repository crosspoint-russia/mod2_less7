<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 03.02.2017
 * Time: 11:33
 */

namespace App;


class AdminDataTable
{
    public $models = [];
    public $functions = [];

    // Отступление от ТЗ:
    // Создайте класс AdminDataTable
    // Его конструктор принимает на вход массив моделей (это будут строки таблицы) и массив функций (это будут столбцы)
    //
    // В текущей реализации первый аргумент массив, либо "обходимый" объект - генератор.

    public function __construct($models, array $functions)
    {
        if ($models instanceof \Traversable || is_array($models)) {
            $this->models = $models;
            $this->functions = $functions;
        } else {
            throw new \Exception('Argument 1 passed to App\AdminDataTable::__construct() must be an array OR implement interface Traversable');
        }

    }

    public function render()
    {
        $view = new View();

        $view->models = $this->models;
        $view->functions = $this->functions;
        $view->columns = array_keys($this->functions);

        return $view->display(__DIR__ . '/../template/admin/datatable.php');
    }
}