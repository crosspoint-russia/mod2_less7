<?php

namespace App;

use App\Exceptions\DbException;

class Db
{

    protected $dbh;

    public function __construct()
    {
        $config = Config::getInstance();
        try {
            $this->dbh = new \PDO($config->data['db']['dsn'], $config->data['db']['user'], $config->data['db']['password']);
        } catch (\PDOException $exception) {

            // разделил создание и бросание исключения для того, чтобы информация об ошибке
            // успела попасть в лог до отправки письма администратору
            $dbe = new DbException($exception->getMessage());

            $postman = new MailerFacade();
            $postman->dbFaultNotification($exception);

            throw $dbe;
        }
    }

    public function query($sql, $data = [], $class = null)
    {
        $sth = $this->dbh->prepare($sql);
        $res = $sth->execute($data);
        if (false === $res) {
            $errors = implode(', ', $sth->errorInfo());
            throw new DbException('Ошибка в запросе: ' . $sql . '; PDO error info: ' . $errors);
        }
        if (null === $class) {
            return $sth->fetchAll(\PDO::FETCH_ASSOC);
        } else {
            return $sth->fetchAll(\PDO::FETCH_CLASS, $class);
        }
    }

    public function queryEach($sql, $data = [], $class = null)
    {
        $sth = $this->dbh->prepare($sql);
        $res = $sth->execute($data);

        if (false === $res) {
            $errors = implode(', ', $sth->errorInfo());
            throw new DbException('Ошибка в запросе: ' . $sql . '; PDO error info: ' . $errors);
        }

        if (null !== $class) {
            $sth->setFetchMode(\PDO::FETCH_CLASS, $class);
        } else {
            $sth->setFetchMode(\PDO::FETCH_ASSOC);
        }

        while ($row = $sth->fetch()) {
            yield $row;
        }
    }

    public function execute($sql, $data = [])
    {
        $sth = $this->dbh->prepare($sql);
        $res = $sth->execute($data);
        if (false === $res) {
            $errors = implode(', ', $sth->errorInfo());
            throw new DbException('Ошибка в запросе: ' . $sql . '; PDO error info: ' . $errors);
        }
        return $res;
    }
    
    public function lastInsertId() {
        return $this->dbh->lastInsertId();
    }

}