<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 24.01.2017
 * Time: 23:38
 */

namespace App\Controllers\Admin;


use App\AController;
use App\Models\Article;
use App\Router;

class Edit extends AController
{
    public function actionDefault()
    {
        if (isset($_GET['id'])) {
            $article = Article::findOneById($_GET['id']);

            if (false === $article) {
                throw new \App\Exceptions\Http404Exception('Страница ' . $_SERVER['REQUEST_URI'] . ' не найдена');
            }

            $this->view = new \App\View();
            $this->view->article = $article;
            $this->view->authors = \App\Models\Author::findAll();
            $this->view->display($this->templatePath . '/admin/edit.php');

        } else {
            throw new \App\Exceptions\Http404Exception('Страница ' . $_SERVER['REQUEST_URI'] . ' не найдена');
        }
    }

    public function actionTwo()
    {
        echo 'hello';
    }
}