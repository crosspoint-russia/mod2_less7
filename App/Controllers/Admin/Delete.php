<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 25.01.2017
 * Time: 0:58
 */

namespace App\Controllers\Admin;


use App\AController;
use App\Router;

class Delete extends AController
{
    public function actionDefault()
    {
        if (isset($_GET['id'])) {
            $article = \App\Models\Article::findOneById($_GET['id']);

            if (false === $article) {
                Router::redirect('/ufo/');
            }

            $article->delete();
        }

        Router::redirect('/admin/?passphrase=iddqd');
    }
}