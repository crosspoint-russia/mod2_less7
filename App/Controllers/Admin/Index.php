<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 24.01.2017
 * Time: 23:07
 */

namespace App\Controllers\Admin;


use App\AController;
use App\Models\Article;
use App\Models\Author;

class Index extends AController
{
    public function actionDefault()
    {
        $this->view->articles = Article::findAll();
        $this->view->authors = Author::findAll();
        $this->view->display($this->templatePath . '/admin/main.php');
    }

    public function actionTwo()
    {
        echo 'say hello';
    }
}