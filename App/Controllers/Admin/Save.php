<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 25.01.2017
 * Time: 0:47
 */

namespace App\Controllers\Admin;


use App\AController;
use App\Models\Article;
use App\Router;

class Save extends AController
{
    public function actionDefault()
    {
        if ('POST' === $_SERVER['REQUEST_METHOD']) {

            if (isset($_POST['id']) && '' !== $_POST['id']) {
                $article = Article::findOneById((int)$_POST['id']);

                if (false === $article) {
                    $article = new Article();
                }

            } else {
                $article = new Article();
            }

            $article->fill($_POST)->save();
        }

        Router::redirect('/admin/?passphrase=iddqd');
    }
}