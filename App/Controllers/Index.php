<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 24.01.2017
 * Time: 22:30
 */

namespace App\Controllers;


use App\Controller;

class Index extends Controller
{
    public function actionDefault()
    {
        $this->view->articles = \App\Models\Article::findRecords(3);
        $this->view->display($this->templatePath . '/main.php');
    }
}