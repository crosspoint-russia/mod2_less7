<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 24.01.2017
 * Time: 22:51
 */

namespace App\Controllers;


use App\Controller;
use App\Router;

class Article extends Controller
{
    public function actionDefault()
    {
        if (true === isset($_GET['id'])) {
            $article = \App\Models\Article::findOneById((int)$_GET['id']);

            if (false === $article) {
                throw new \App\Exceptions\Http404Exception('Страница ' . $_SERVER['REQUEST_URI'] . ' не найдена');
            }

            $this->view->article = $article;
            $this->view->display($this->templatePath . '/article.php');

        } else {
            throw new \App\Exceptions\Http404Exception('Страница ' . $_SERVER['REQUEST_URI'] . ' не найдена');
        }
    }
}