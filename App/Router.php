<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 24.01.2017
 * Time: 21:45
 */

namespace App;


class Router
{
    public static function locate()
    {
        /* Предварительная подготовка к маршрутизации */
        
        $config = Config::getInstance();                // загрузка конфига (для отладки)

        $nsPrefix = '\\App\\Controllers\\';             // префикс для формирования полного имени класса

        $url = parse_url($_SERVER['REQUEST_URI']);      // отделяем url от get параметров
        
        $cleanRoute = trim($url['path'], '/');          // предварительная чистка url
        $explodedRoute = explode('/', $cleanRoute);     // и его разбивка в массив

        $askedRoute = array_map(function($item){        // строим массив Отформатированных элементов запрошенного маршрута
            return ucfirst($item);
        }, $explodedRoute);

        $innerRoute = __DIR__ . '/Controllers/' . implode('/', $askedRoute);    // путь к потенциально существующей папке контроллера
        $possibleExistingController = implode('\\', $askedRoute);   // имя потенциально существующего контроллера

        /* Блок условий маршрутизации */

        // Проверяем не пересекается ли маршрут с существующей папкой контроллера
        if (file_exists($innerRoute)) {

            // если полностью пересекается с папкой существующего контроллера, берем из неё контроллер индекс
            if (!empty($askedRoute[0])) {
                $controllerName = implode('\\', $askedRoute) . '\\Index';
            } else {
                // контроллер индекс для корня сайта
                $controllerName = 'Index';
            }

        } else {

            // Проверяем не пересекается ли маршрут с существующим классом контроллера
            // Если полностью пересекается с существующим классом, считаем этот класс искомым маршрутом
            if (class_exists($nsPrefix . $possibleExistingController)) {
                $controllerName = $possibleExistingController;
            } else {
                $askedAction = array_pop($askedRoute);  // извлекаем action из массива
                $controllerName = implode('\\', $askedRoute);

                // если после извлечения action из остатков не получается собрать имя существующего контроллера,
                // считаем остатки маршрута именем папки контроллера и запускаем контроллер индекс.
                if (!class_exists($nsPrefix . $controllerName)) {
                    $controllerName = $controllerName . '\\Index';
                }
            }

        }

        $actionName = $askedAction ?? 'Default';            // Если установлен экшн, берем его иначе считаем запрошенным Default
        
        $controllerClassName = $nsPrefix . $controllerName; // Формируем полное имя класса
        
        if (class_exists($controllerClassName)) {           // проверка на существование класса перед запуском
            $result['controller'] = $controllerClassName;
            $result['action'] = $actionName;
            return $result;
        } else {
            return false;
        }
    }

    /* метод для перенаправления внутри приложения */
    public static function redirect($url)
    {
        header('Location: ' . $url);
        die;
    }

}