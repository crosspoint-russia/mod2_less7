<?php

namespace App;

use App\Models\Article;
use App\Models\Author;
use CrosspointRussia\Multiexception\MultiException;

/**
 * @property int $id
 */

abstract class Model
{

    use MagicTrait;

    public static $foreignLinks = [];
    public static $fillable = [];

    // дополняем магию связями с другими моделями
    public function __get($name)
    {
        if (in_array($name, array_keys(static::$foreignLinks))) {

            $linkId = static::$foreignLinks[$name]['on'];

            if (null !== $this->data[$linkId]) {
                return static::$foreignLinks[$name]['class']::findOneById($this->data[$linkId]);
            } else {
                return false;
            }
        }

        if (isset($this->data[$name])) {
            return $this->data[$name];
        } else {
            return null;
        }

    }

    // перевел "большую" выборку на queryEach
    public static function findAll()
    {
        $db = new Db();
        $sql = 'SELECT * FROM ' . static::$table;
        return $db->queryEach($sql, [], static::class);
    }

    /**
     * @param $id
     * @return Article|Author|bool
     */
    public static function findOneById(int $id)
    {
        $db = new Db();
        $sql = 'SELECT * FROM ' . static::$table . ' WHERE id=:id';
        $result = $db->query($sql, [':id'=>$id] , static::class);

        if (true === empty($result)) {
            return false;
        } else {
            return $result[0];
        }
    }

    public static function findRecords(int $limit)
    {
        $db = new Db();

        $sql = 'SELECT * FROM ' . static::$table . ' ORDER BY id DESC LIMIT ' . $limit;

        $result = $db->query($sql, [] , static::class);

        return $result;
    }

    public static function countAll()
    {
        $db = new Db();
        $sql = 'SELECT COUNT(*) AS num FROM ' . static::$table;
        return (int)$db->query($sql, [], static::class)[0]->num;
    }

    protected function update()
    {
        $sets = [];
        $data = [];
        foreach ($this->data as $key => $value) {
            $data[':' . $key] = $value;
            if ('id' == $key) {
                continue;
            }
            $sets[] = $key . '=:' . $key;
        }

        $db = new Db();
        $sql = 'UPDATE ' . static::$table . ' 
        SET ' . implode(',', $sets) . ' 
        WHERE id=:id';
        return $db->execute($sql, $data);
    }

    // TODO: insert, delete, save

    protected function insert()
    {
        $db = new Db();
        $data = [];
        $keys = [];

        // готовим массивы для запроса
        foreach ($this->data as $key => $value) {
            $data[':' . $key] = $value;
            $keys[] = $key;
        }

        // строим SQL запрос
        $sql = 'INSERT INTO ' . static::$table . '
                (' . implode(',', $keys) .') 
                VALUES (' . implode(',', array_keys($data)) .')';
        
        $result = $db->execute($sql, $data);

        if (false !== $result) {
            $this->id = (int)$db->lastInsertId();
            return true;
        } else {
            return false;
        }

    }

    public function delete()
    {
        $db = new Db();
        $sql = 'DELETE FROM ' . static::$table . ' WHERE id=:id';
        return $db->execute($sql, [':id'=>$this->id]);
    }

    public function save():bool
    {
        if (!isset($this->id)) {
            return $this->insert();
        } else {
            return $this->update();
        }
    }

    public function fill(array $data)
    {
        $errors = new MultiException();

        foreach (static::$fillable as $field) {

            if (isset($data[$field])) {

                // Проба валидаторов
                $validator = 'validate' . ucfirst($field);
                if (method_exists($this, $validator)) {
                    $result = $this->$validator($data[$field]);
                    if (false === $result) {
                        $errors->add(new \Exception('Invalid ' . $field));
                        continue;
                    }
                }
                
                $this->$field = $data[$field];
            } else {
                $errors->add(new \Exception('Не передано поле ' . $field));
            }

        }

        if (!$errors->isEmpty()) {
            throw $errors;
        }
        return $this;
    }

    public function isNew()
    {
        return null === $this->id;
    }


}