<?php

namespace App\Models;

use App\Model;

/**
 * @property string $title Заголовок новости
 * @property string $text Полный текст новости
 * @property int $author_id Связь с моделью Author.
 * @property Author|false $author Модель Автора или false
 */

class Article
    extends Model
{

    public static $table = 'news';

    // массив заполняемых полей
    public static $fillable = [
        'title',
        'text',
        'author_id',
    ];

    // массив для внешних связей с другими моделями
    public static $foreignLinks = [
        'author' => [
            'on' => 'author_id',
            'class' => Author::class,
        ]
    ];
    
    // Проба валидаторов

    protected function validateTitle($title)
    {
        $rule = '~[а-яА-Я]{3,}~u'; // 3 или более кириллических символа
        return (bool) preg_match($rule, $title);
    }
    
    protected function validateText($text)
    {
        if (strlen($text) >= 50) {
            return true;
        } 
        return false;
    }
    
    protected function validateAuthor_id($author_id)
    {
        return '0' !== $author_id;
    }

}