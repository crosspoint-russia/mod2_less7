<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 01.02.2017
 * Time: 20:35
 */

namespace App;


class MailerFacade
{
    protected $postBox = null;

    public function __construct()
    {
        $config = Config::getInstance();

        $mailServer = \Swift_SmtpTransport::newInstance($config->data['mail']['smtp'], $config->data['mail']['port'], $config->data['mail']['encType'])
            ->setUsername($config->data['mail']['user'])
            ->setPassword($config->data['mail']['pass']);

        $this->postBox = \Swift_Mailer::newInstance($mailServer);
    }

    // метод для отправки писем от "имени" приложения
    public function sendMessage($to, $subj, $body, array $attachments = [])
    {
        $config = Config::getInstance();

        $message = \Swift_Message::newInstance($subj)
            ->setFrom($config->data['mail']['user'])
            ->setTo($to)
            ->setContentType('text/html')
            ->setBody($body);

        foreach ($attachments as $attachment) {
            $message->attach(\Swift_Attachment::fromPath($attachment));
        }

        $this->postBox->send($message);
    }

    // метод для отправки уведомлений о внештатных ситуациях с БД
    // вложение архивного лога в письмо просто для того, чтобы посмотреть как это работает.
    public function dbFaultNotification(\PDOException $exception)
    {
        $config = Config::getInstance();

        $view = new View();
        $view->error = $exception;

        $message = $view->render(__DIR__ . '/../template/mail/dbFault.php');

        $this->sendMessage('it@crosspointrussia.ru', 'Emergency DB notification!', $message, [$config->data['logPath']]);
    }
}